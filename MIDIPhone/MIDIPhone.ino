#include <Key.h>
#include <Keypad.h>
#include <MIDIUSB.h>

const int modPin = 0;

const byte rows = 4;
const byte cols = 3;
char keys[rows][cols] = {
  {56, 57, 58},
  {59, 60, 61},
  {62, 63, 64},
  {65, 66, 67}
};

byte rowPins[rows] = {2, 3, 4, 5};
byte colPins[cols] = {6, 7, 8};

int modValuePrev = 0;

Keypad keypad = Keypad(makeKeymap(keys), rowPins, colPins, rows, cols);

void setup() {
  keypad.addEventListener(keypadEvent);
}

void loop() {
  keypad.getKey();

  int modValue = analogRead(modPin);
  int modDiff = abs(modValue - modValuePrev);

  if (modDiff >= 3) {
    modValuePrev = modValue;
    int modRelative = map(modValue, 0, 1023, 0, 127);
    setModulation(modRelative);
  }
}

void keypadEvent(KeypadEvent key) {
  switch(keypad.getState()) {
    case PRESSED:
      playMidi(key);
      break;
    case RELEASED:
      stopMidi(key);
      break;
  }
}

// 0x0B control
void setModulation(byte modulationAmount) {
  midiEventPacket_t mod = {0x0B, 0xB0 | 0, 1, modulationAmount};
  MidiUSB.sendMIDI(mod);
  MidiUSB.flush();
}

// 0x09 note on
void playMidi(byte pitch) {
  midiEventPacket_t noteOn = {0x09, 0x90 | 0, pitch, 100};
  MidiUSB.sendMIDI(noteOn);
  MidiUSB.flush();
}

// 0x08 note off
void stopMidi(byte pitch) {
  midiEventPacket_t noteOff = {0x08, 0x80 | 0, pitch, 100};
  MidiUSB.sendMIDI(noteOff);
  MidiUSB.flush();
}

